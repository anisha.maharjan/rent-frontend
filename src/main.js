import Vue from 'vue'
import App from './App.vue'

// axios
import axios from "axios"
Vue.prototype.$axios = axios;

// vform
import { Form, HasError, AlertError } from 'vform'
Vue.prototype.$Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

// vue router
import router from "./router";

// components
Vue.component("Sidenav", require("./components/Includes/Sidenav.vue").default);
Vue.component("Topnav", require("./components/Includes/Topnav.vue").default);
Vue.component("Footer", require("./components/Includes/Footer.vue").default);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
