import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
    { path: '/', name: 'showDashboard', component: require('./components/Pages/Dashboard.vue').default },

    // auth
    { path: '/login', name: 'showLogin', component: require('./components/Pages/Auth/Login.vue').default },
    { path: '/register', name: 'showRegister', component: require('./components/Pages/Auth/Register.vue').default },

    // users
    { path: '/users', name: 'showUsers', component: require('./components/Pages/User/Users.vue').default },
]
  
const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router